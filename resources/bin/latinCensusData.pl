#!/usr/bin/perl -w
#use strict;
use warnings;
use Switch;
use Data::Dumper;
use DBI;

# Global variables:
my %data = (); 
my %universities = (); 
my %countries = ();
my %cities = ();
my $dsn = "DBI:mysql:latincensus";
my $dbUser = "census";
my $dbh = "";

# Functions:
sub dbConnect{

    $dbh = DBI->connect($dsn, $dbUser, $dbUser, 
           {'RaiseError' => 1, AutoCommit => 0, mysql_server_prepare => 1}) or 
           die "Couldn't connect to db: " . DBI->errstr;
    return $dbh;
}

sub dbClose {
    $dbh = shift;
    $dbh->disconnect
        or warn "Disconnection failed: $DBI::errstr\n";
}

sub loadCities {
    my $fh = shift;
    my $hashRef = shift;
    my $country = shift;
    my $citiesString = "";

    while (my $row = <$fh>) {
        chomp $row;
        if (($row =~ m/\w+/) && (!($row =~ m/GAP/))) {
            $citiesString = $citiesString.$row."|";
  	} else {
            # Read blank line or 'GAP' line, return...
            chop $citiesString; 
            $hashRef->{$country} = $citiesString;
            return %{$hashRef};
	}
    }
}

sub loadCountries {
    my $file = shift;
    my $continent = shift; 
    my %countriesToCities = ();
    
    open(my $fh, '<:encoding(UTF-8)', $file)
        or die "Could not open file '$file' $!";
 
    while (my $row = <$fh>) {
        chomp $row;
	if ($row =~ m/city names/) {
            my @parts = (split(/:/, $row))[0];
            my $parts = join(' ', @parts);
            # If line fragment contains "[Under" then erase it,
            # If line fragment now contains "[In" then erase it,
            if ($parts =~ m/\[Under/) { $parts =~ s/Under //; }
            if ($parts =~ m/\[For /) { $parts =~ s/For //; }
            if ($parts =~ m/\[In /) { $parts =~ s/\[In //; }
            my $country = $parts;
	    %countriesToCities = loadCities($fh, \%countriesToCities, $country);
	}
    }
    close($fh);
    $data{$continent} = \%countriesToCities;
}

sub loadUniversities {
    my $file = shift;
    my $school = "";
    my $country = "";
    
    open(my $fh, '<:encoding(UTF-8)', $file)
        or die "Could not open file '$file' $!";
 
    while (my $row = <$fh>) {
        chomp $row;
	if ($row =~ m/COUNTRY:/) {
            # let's just look at everything after the ':'
            my @parts = (split(/:/, $row))[1];
            my $parts = join(' ', @parts);
            # If line fragment contains " for in " then erase it,
            if ($parts =~ m/ for in /i) { $parts =~ s/ for in //i; }
            # If line fragment now contains an ending ']', erase it.
            if ($parts =~ m/\]/) { $parts =~ s/\]//; }
            $country = $parts;
	}
        elsif (($row =~ m/\w+/) && (!($row =~ m/GAP/))) {
            $school = $school.$row."|";
  	} elsif ($row =~ m/GAP/) {
            # Read blank line or 'GAP' line, return...
            chop $school; 
            $universities{$country} = $school;
            $school = ""; 
            next;
	} else {
             next;
        }
         
    }
    close($fh);
}

sub testHash {
    my %datahash = ();
    my %testhash = ();
    $testhash{"a"} = 'Alex';
    $testhash{"b"} = 'Bryan';
    $testhash{"c"} = 'Chuck';
    my $testhash_ref = \%testhash;
    $datahash{'test'} = $testhash_ref;

    while (my ($key,$value) = each(\%datahash)) {
        print "YO: The value is $value\n";
        for (keys($value)) {
            print "YEE: KEY is $_\n";
            print "YEE: VALUE is $value->{$_}\n";
        }
    }
}

sub addCountries {
    # Insert the countries into the 'countries' table.
    my ($countriesHashRef, $dbh) = @_;

    while (my ($countryName,$continentId) = each($countriesHashRef)) {
        my $insert_handle = 
            $dbh->prepare_cached('INSERT INTO countries (name, continent_id) VALUES (?,?)'); 
        die "Couldn't prepare queries; aborting" unless defined $insert_handle;
        my $success = 1;
        $success &&= $insert_handle->execute($countryName, $continentId);
print "INSERT INTO COUNTRIES (name, continent_id) VALUES ($countryName, $continentId)\n";
print "Success is $success\n";
        my $result = ($success ? $dbh->commit : $dbh->rollback);
        unless ($result) { 
print "The following SQL failed:\n";
print "INSERT INTO COUNTRIES (name, continent_id) VALUES ($countryName, $continentId)\n";
print "Success is $success\n";
            die "Couldn't finish transaction: " . $dbh->errstr 
        }
    }
    return $success;
}

sub addCities {
    my ($country, $countryId, $continentName, $dbh) = @_;
    my $city = "";
    my $cityName = "";
    my %cities = ();
    my $dataHashRef = \%data;

    # Get all the cities for the given country...
    # Since perl managed to break the "keys" operator for hash refs, 
    # we'll have to do this the hard way.
    while (my ($key, $value) = each %data){
        if ($key eq $continentName) { 
            while (my ($countryKey, $citiesValue) = each $value){
                if ($countryKey eq $country){
                    @cities = split('\|', $citiesValue);
                    foreach $cityName (@cities) {
                        my $insert_handle = 
                  $dbh->prepare_cached('INSERT INTO cities (name, country_id) VALUES (?,?)'); 
                        die "Couldn't prepare queries; aborting" unless defined $insert_handle;
                        my $success = 1;
                        $success &&= $insert_handle->execute($cityName, $countryId);
             print "INSERT INTO cities (name, country_id) VALUES ($cityName,$countryId)\n";
             print "Success is $success\n";
                        my $result = ($success ? $dbh->commit : $dbh->rollback);
                        unless ($result) { 
             print "The following SQL failed:\n";
             print "INSERT INTO cities (name, country_id) VALUES ($cityName,$countryId)\n";
             print "Success is $success\n";
                            die "Couldn't finish transaction: " . $dbh->errstr 
                        }
                    }
                }
            }
        }
    }
    return $success;
}

sub addUniversities {
    my ($schools, $dbh) = @_;
    my @univs = ();

    my $countryIds = 
        $dbh->selectall_hashref(q/select id, name from countries/, q/name/ );

    while (my ($countryName, my $schoolsString) = each $schools) {
        #Get the country id for $countryName...
        if ($countryIds->{$countryName}) { 
        #Get the country id for $countryName...
            @univs = split('\|', $schoolsString);
            foreach my $school (@univs) {
                my $insert_handle = 
                  $dbh->prepare_cached('INSERT INTO universities (name, country_id) VALUES (?,?)'); 
                die "Couldn't prepare queries; aborting" unless defined $insert_handle;
                my $success = 1;
                $success &&= $insert_handle->execute($school, $countryIds->{$countryName}->{id});
                my $result = ($success ? $dbh->commit : $dbh->rollback);
                unless ($result) { 
                    print "The following SQL failed:\n";
                    print "INSERT INTO cities (name, country_id) VALUES ($cityName,$countryId)\n";
                        die "Couldn't finish transaction: " . $dbh->errstr 
                }
            }
	}  
    }
}

sub showData {
    my ($aHash) = @_;

    print Dumper($aHash);
    return;
}

# Main
my $inpath = "../data/files";
my $outpath = "../data/dml";
my $success = 0;

# Load the data files into the %data hash...
my @files = `ls $inpath`;
    foreach my $file (@files) {
        chomp($file);

    	switch ($file) {
            case "citiesAfrica.txt" {
                loadCountries($inpath."/".$file, 'Africa');
            }
            case "citiesAsia.txt" {
                loadCountries($inpath."/".$file, 'Asia');
            }
            case "citiesEurope.txt" {
                loadCountries($inpath."/".$file, 'Europa');
            }
            case "citiesNorthAmerica.txt" {
                loadCountries($inpath."/".$file, 'America Septentrionali');
            }
            case "citiesSouthAmerica.txt" {
                loadCountries($inpath."/".$file, 'America Meridiana');
            }
            case "citiesUniversities.txt" {
                loadUniversities($inpath."/".$file);
            }
            else {
                print "Found uninteresting file $file\n";
            }
      }
}

$dbh = dbConnect();

# Get the continent ids.  Hash ref to hold continents data from db:
my $dbContinents = 
    $dbh->selectall_hashref(q/select id, name from continents/, q/id/ );

#  For each continent, get the countries associated with that continent
#  and insert the country name and continent id into the countries table:
foreach ( keys %$dbContinents ) {
    %countries = ();
    my $continentName = $dbContinents->{$_}->{name};
    my $continentId = $dbContinents->{$_}->{id};
    foreach $country (sort keys $data{$continentName}) {
        $countries{$country} = $continentId;
    }

    $success = addCountries(\%countries, $dbh);
    my $dbCountryIds = 
        $dbh->selectall_hashref(q/select id, name from countries/, q/name/ );

    foreach $country (sort keys %$dbCountryIds) {
        my $countryId = $dbCountryIds->{$country}->{id}; 
        $success = addCities($country, $countryId, $continentName, $dbh);
    }
}

$success = addUniversities(\%universities, $dbh);

dbClose($dbh);
exit 0;

