<!DOCTYPE html>
<html>
    <head>
        <style type="text/css">
         html {
             width: 100%;
             height: 100%;
         }
         body {
             width: 100%;
             height: 100%;
             margin : 0;
         }
         .outer-container {
             display: table;
             width: 100%;
             height: 100%;
         }
         .inner-container {
             display: table-cell;
             vertical-align: middle;
             text-align: center;
         }
         .centered-content {
             display: inline-block;
             text-align: left;
         }
        </style>
    </head>
    <body>
        <div class="outer-container">
            <div class="inner-container">
                <div class="centered-content">
                    <h2>Gratias tibi agimus, quod censui Latino nomen dedisti.</h2>
                </div>
            </div>
        </div>
    </body>
</html>
