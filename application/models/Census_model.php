<?php
class Census_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function get_users($user = '') {
        if (empty($user))
        {
                $query = $this->db->get('Users');
                return $query->result_array();
        }

        $query = $this->db->get_where('users', array('fname' => $user['fname']));
        return $query->row_array();
    }

    public function get_user($id = '') {
        $query = $this->db->get_where('users', array('id' => $user['id']));
        return $query->row_array();
    }

    public function set_user_test() {
        $this->load->helper('url');
        $data = array(
          'FName'                 => 'Chad',
          'LName'                 => 'Peters',
          'Age'                   => '1984',
          'Gender'                => 'male'
        );
        var_dump($data);
        $this->db->insert('Users', $data);
    }

    public function save() {
        return $this->set_user();
    }

    public function set_user() {
        $data = array();

        $data['FName'] = $this->input->post('FName');
        $data['LName'] = $this->input->post('LName');
        if ($this->input->post('OtherGender')) {$data['Gender']=$this->input->post('OtherGender');}
        else {$data['Gender']=$this->input->post('Gender');}

        if ($this->input->post('OtherEthnicity')) {$data['Ethnicity']=$this->input->post('OtherEthnicity');}
        else {$data['Ethnicity']=$this->input->post('Ethnicity');}

        $data['Age'] = $this->input->post('Age');
        $data['ContinentOfBirthId'] = $this->input->post('ContinentOfBirthId');
        $data['CountryOfBirthId'] = $this->input->post('CountryOfBirthId');
        $data['ContinentOfResidenceId'] = $this->input->post('ContinentOfResidenceId');
        $data['CountryOfResidenceId'] = $this->input->post('CountryOfResidenceId');

        $data['CityOfResidence'] = $this->input->post('CityOfResidenceId');
        if ($this->input->post('otherCityOfResidence')) {
          $data['CityOfResidence']=$this->input->post('otherCityOfResidence');
        }

	$data['Profession']= $this->input->post('Profession');
        if ($this->input->post('OtherProfession')) {
	    $data['Profession']=$this->input->post('OtherProfession');
  	}

        if ($this->input->post('RetiredProfession')) {
            $data['Profession']=$this->input->post('RetiredProfession');
        } 

        if ($this->input->post('JCBelief')) {$data['Religion']=$this->input->post('JCBelief');}
        elseif ($this->input->post('Religion') == 'other') {$data['Religion']=$this->input->post('OtherReligion');} 
        else {$data['Religion']=$this->input->post('Religion');}

        /* This is where you derive the other languages answer */
        $otherLanguages = '';
        if ($this->input->post('other-text-box-X')) {
            $data['Language']=$this->input->post('other-text-box-X');
        } else {
            if ($this->input->post('checkbox-1')) {
                $otherLanguages= "||" . $this->input->post('checkbox-1') . '#';
                $otherLanguages.=$this->input->post('radio-1') ;
            }
            if ($this->input->post('checkbox-2')) {
                $otherLanguages.= "||" . $this->input->post('checkbox-2') . "#";
                $otherLanguages.=$this->input->post('radio-2');
            }
            if ($this->input->post('checkbox-3')) {
                $otherLanguages.= "||" . $this->input->post('checkbox-3') . "#";
                $otherLanguages.=$this->input->post('radio-3');
            }
            if ($this->input->post('checkbox-4')) {
                $otherLanguages.= "||" . $this->input->post('checkbox-4') . "#";
                $otherLanguages.=$this->input->post('radio-4');
            }
            if ($this->input->post('checkbox-5')) {
                $otherLanguages.= "||" . $this->input->post('checkbox-5') . "#";
                $otherLanguages.=$this->input->post('radio-5');
            }
            if ($this->input->post('checkbox-6')) {
                $otherLanguages.= "||" . $this->input->post('checkbox-6') . "#";
                $otherLanguages.=$this->input->post('radio-6');
            }
            if ($this->input->post('checkbox-7')) {
                $otherLanguages.= "||" . $this->input->post('checkbox-7') . "#";
                $otherLanguages.=$this->input->post('radio-7');
            }
            if ($this->input->post('checkbox-8')) {
                $otherLanguages.= "||" . $this->input->post('checkbox-8') . "#";
                $otherLanguages.=$this->input->post('radio-8');
            }
            if ($this->input->post('checkbox-9')) {
                $otherLanguages.= "||" . $this->input->post('checkbox-9') . "#";
                $otherLanguages.=$this->input->post('radio-9');
            }
            if ($this->input->post('checkbox-10')) {
                $otherLanguages.= "||" . $this->input->post('checkbox-10') . "#";
                $otherLanguages.=$this->input->post('radio-10');
            }
            if ($this->input->post('checkbox-11')) {
                $otherLanguages.= "||" . $this->input->post('checkbox-11') . "#";
                $otherLanguages.=$this->input->post('radio-11');
            }
            if ($this->input->post('checkbox-12')) {
                $otherLanguages.= "||" . $this->input->post('checkbox-12') . "#";
                $otherLanguages.=$this->input->post('radio-12');
            }
            if ($this->input->post('checkbox-13')) {
                $otherLanguages.= "||" . $this->input->post('checkbox-13') . "#";
                $otherLanguages.=$this->input->post('radio-13');
            }
            if ($this->input->post('checkbox-14')) {
                $otherLanguages.= "||" . $this->input->post('checkbox-14') . "#";
                $otherLanguages.=$this->input->post('radio-14');
            }
            if ($this->input->post('checkbox-15')) {
                $otherLanguages.= "||" . $this->input->post('checkbox-15') . "#";
                $otherLanguages.=$this->input->post('radio-15');
            }
            if ($this->input->post('checkbox-16')) {
                $otherLanguages.= "||" . $this->input->post('checkbox-16') . "#";
                $otherLanguages.=$this->input->post('radio-16');
            }
            if ($this->input->post('checkbox-17')) {
                $otherLanguages.= "||" . $this->input->post('checkbox-17') . "#";
                $otherLanguages.=$this->input->post('radio-17');
            }
            if ($this->input->post('checkbox-18')) {
                $otherLanguages.= "||" . $this->input->post('checkbox-18') . "#";
                $otherLanguages.=$this->input->post('radio-18');
            }
            if ($this->input->post('checkbox-19')) {
                $otherLanguages.= "||" . $this->input->post('checkbox-19') . "#";
                $otherLanguages.=$this->input->post('radio-19');
            }
            if ($this->input->post('checkbox-20')) {
                $otherLanguages.= "||" . $this->input->post('checkbox-20') . "#";
                $otherLanguages.=$this->input->post('radio-20');
            }
            if ($this->input->post('checkbox-21')) {
                $otherLanguages.= "||" . $this->input->post('checkbox-21') . "#";
                $otherLanguages.=$this->input->post('radio-21');
            }
            if ($this->input->post('checkbox-22')) {
                $otherLanguages.= "||" . $this->input->post('checkbox-22') . "#";
                $otherLanguages.= $this->input->post('radio-22');
            }
            $data['Language']=$otherLanguages;
        }
        $data['AgeStart'] = $this->input->post('AgeStart');

        if ($this->input->post('OtherHowLearn')) {$data['HowLearn']=$this->input->post('OtherHowLearn');}
        else {$data['HowLearn']=$this->input->post('HowLearn');}

        // Derive the Question 13 answer
        $howUse = '';
        if ($this->input->post('how-checkbox-1')) {
            $howUse.=$this->input->post('how-checkbox-1') . "|";
        }
        if ($this->input->post('how-checkbox-2')) {
            $howUse.=$this->input->post('how-checkbox-2') . "|";
        }
        if ($this->input->post('how-checkbox-3')) {
            $howUse.=$this->input->post('how-checkbox-3') . "|";
        }
        if ($this->input->post('how-checkbox-4')) {
            $howUse.=$this->input->post('how-checkbox-4') . "|";
        }
        $data['HowUse'] = $howUse;
        $data['YearsRead'] = $this->input->post('YearsRead');
        $data['YearsSpeak'] = $this->input->post('YearsSpeak');
        $data['YearsWrite'] = $this->input->post('YearsWrite');
        $data['YearsListen'] = $this->input->post('YearsListen');

        if ($this->input->post('otherSchool')){
          $data['OtherUniversity']=$this->input->post('otherSchool');}
        elseif ($this->input->post('onlineCourse')){
            $data['OnlineCourse']=$this->input->post('onlineCourse');
        }
        else {
            if ($this->input->post('univContinent')) {
                $data['UniversityId']=$this->input->post('univContinent');
            } 
        }

        /* This is where you derive the other languages answer */
        $seminars = '';
        if ($this->input->post('seminar-checkbox-1')) {
            $seminars.=$this->input->post('seminar-checkbox-1') . "#";
            $seminars.=$this->input->post('seminar-radio-1') . "||";
        }
        if ($this->input->post('seminar-checkbox-2')) {
            $seminars.=$this->input->post('seminar-checkbox-2') . "#";
            $seminars.=$this->input->post('seminar-radio-2') . "||";
        }
        if ($this->input->post('seminar-checkbox-3')) {
            $seminars.=$this->input->post('seminar-checkbox-3') . "#";
            $seminars.=$this->input->post('seminar-radio-3') . "||";
        }
        if ($this->input->post('seminar-checkbox-4')) {
            $seminars.=$this->input->post('seminar-checkbox-4') . "#";
            $seminars.=$this->input->post('seminar-radio-4') . "||";
        }
        if ($this->input->post('seminar-checkbox-5')) {
            $seminars.=$this->input->post('seminar-checkbox-5') . "#";
            $seminars.=$this->input->post('seminar-radio-5') . "||";
        }
        if ($this->input->post('seminar-checkbox-6')) {
            $seminars.=$this->input->post('seminar-checkbox-6') . "#";
            $seminars.=$this->input->post('seminar-radio-6') . "||";
        }
        if ($this->input->post('seminar-checkbox-7')) {
            $seminars.=$this->input->post('seminar-checkbox-7') . "#";
            $seminars.=$this->input->post('seminar-radio-7') . "||";
        }
        if ($this->input->post('seminar-checkbox-8')) {
            $seminars.=$this->input->post('seminar-checkbox-8') . "#";
            $seminars.=$this->input->post('seminar-radio-8') . "||";
        }
        if ($this->input->post('seminar-checkbox-9')) {
            $seminars.=$this->input->post('seminar-checkbox-9') . "#";
            $seminars.=$this->input->post('seminar-radio-9') . "||";
        }
        if ($this->input->post('seminar-checkbox-10')) {
            $seminars.=$this->input->post('seminar-checkbox-10') . "#";
            $seminars.=$this->input->post('seminar-radio-10') . "||";
        }
        if ($this->input->post('seminar-checkbox-11')) {
            $seminars.=$this->input->post('seminar-checkbox-11') . "#";
            $seminars.=$this->input->post('seminar-radio-11') . "||";
        }
        if ($this->input->post('seminar-checkbox-12')) {
            $seminars.=$this->input->post('seminar-checkbox-12') . "#";
            $seminars.=$this->input->post('seminar-radio-12') . "||";
        }
        if ($this->input->post('seminar-checkbox-13')) {
            $seminars.=$this->input->post('seminar-checkbox-13') . "#";
            $seminars.=$this->input->post('seminar-radio-13') . "||";
        }
        if ($this->input->post('seminar-checkbox-14')) {
            $seminars.=$this->input->post('seminar-checkbox-14') . "#";
            $seminars.=$this->input->post('seminar-radio-14') . "||";
        }
        if ($this->input->post('seminar-checkbox-15')) {
            $seminars.=$this->input->post('seminar-checkbox-15') . "#";
            $seminars.=$this->input->post('seminar-radio-15') . "||";
        }
        if ($this->input->post('seminar-checkbox-16')) {
            $seminars.=$this->input->post('seminar-checkbox-16') . "#";
            $seminars.=$this->input->post('seminar-radio-16') . "||";
        }
        if ($this->input->post('seminar-checkbox-17')) {
            $seminars.=$this->input->post('seminar-checkbox-17') . "#";
            $seminars.=$this->input->post('seminar-radio-17') . "||";
        }
        if ($this->input->post('seminar-checkbox-18')) {
            $seminars.=$this->input->post('seminar-checkbox-18') . "#";
            $seminars.=$this->input->post('seminar-radio-18') . "||";
        }
        if ($this->input->post('seminar-checkbox-19')) {
            $seminars.=$this->input->post('seminar-checkbox-19') . "#";
            $seminars.=$this->input->post('seminar-radio-19') . "||";
        }
        if ($this->input->post('seminar-checkbox-20')) {
            $seminars.=$this->input->post('seminar-checkbox-20') . "#";
            $seminars.=$this->input->post('seminar-radio-20') . "||";
        }
        if ($this->input->post('other-seminars-text-box')) {
            $seminars.=$this->input->post('other-seminars-text-box') . "#";
        }
        $data['SeminarsAttended']=$seminars;

        if ($this->input->post('OtherTextbook')) {$data['Textbook']=$this->input->post('OtherTextbook');}
        else {$data['Textbook']=$this->input->post('Textbook');}

        $data['WhereLearn'] = '';
        if ($this->input->post('Use1')) {$data['WhereLearn']=$this->input->post('Use1');}
        if ($this->input->post('Use2')) {$data['WhereLearn'].= "|".$this->input->post('Use2');}
        if ($this->input->post('Use3')) {$data['WhereLearn'].= "|".$this->input->post('Use3');}
        if ($this->input->post('Use4')) {$data['WhereLearn'].= "|".$this->input->post('Use4');}
        if ($this->input->post('Use5')) {$data['WhereLearn'].= "|".$this->input->post('Use5');}
        if ($this->input->post('Use6')) {$data['WhereLearn'].= "|".$this->input->post('Use6');}
        if ($this->input->post('Use7')) {$data['WhereLearn'].= "|".$this->input->post('Use7');}
        if ($this->input->post('Use8')) {$data['WhereLearn'].= "|".$this->input->post('Use8');}
        if ($this->input->post('Use9')) {$data['WhereLearn'].= "|".$this->input->post('Use9');}
        if ($this->input->post('Use10')) {$data['WhereLearn'].= "|".$this->input->post('Use10');}
        if ($this->input->post('Use11')) {$data['WhereLearn'].= "|".$this->input->post('Use11');}
        if ($this->input->post('Use12')) {$data['WhereLearn'].= "|".$this->input->post('Use12');}
        if ($this->input->post('Use13')) {$data['WhereLearn'].= "|".$this->input->post('Use13');}
        if ($this->input->post('otherUse')) {$data['WhereLearn'].= "|".$this->input->post('otherUse');}

        $data['EmailAddress'] = $this->input->post('EmailAddress');
        $data['Comments'] = $this->input->post('comment');

        if (! $this->db->insert('Users', $data)) {
            $error = $this->db->error_message();
            var_dump('DB Error:'.$error);
        }
    }

    public function get_universities() {
      $query = $this->db->get('universities');
      return $query->result_array();
    }

    public function get_universities_by_country_id($id) {
        $clause = array('country_id' => $id);
        $this->db->select('name, id');
        $this->db->where($clause);
        $this->db->order_by("name", "asc");
        $this->db->from('universities');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_continents() {
      //$query = $this->db->get('continents');
      $this->db->select('name, id');
      $this->db->from('continents');
      $this->db->order_by("name", "asc");
      $query = $this->db->get();
      return $query->result_array();
    }

    public function get_countries_by_continent_id($id) {
        $clause = array('continent_id' => $id);
        $this->db->select('name, id');
        $this->db->where($clause);
        $this->db->order_by("name", "asc");
        $this->db->from('countries');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_cities_by_country_id($id) {
        $clause = array('country_id' => $id);
        $this->db->select('name, id');
        $this->db->where($clause);
        $this->db->order_by("name", "asc");
        $this->db->from('cities');
        $query = $this->db->get();
        return $query->result_array();
    }
}
