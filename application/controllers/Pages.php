<?php
class Pages extends CI_Controller {

    function __construct() {
      parent::__construct();
      // path to simple_html_dom
    }

    public function view($page = 'home') {
        if ( ! file_exists(APPPATH.'views/pages/'.$page.'.php'))
        {
                // Whoops, we don't have a page for that!
                show_404();
        }
        $data['baseUrl'] = APPPATH;

        $data['title'] = 'CENSVS LATINVS'; // Capitalize the first letter
        $this->load->view('templates/header', $data);
        $this->load->view('pages/'.$page, $data);
        $this->load->view('templates/footer', $data);
    }
}
