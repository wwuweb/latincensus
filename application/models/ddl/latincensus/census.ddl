CREATE TABLE countries (
    id int(11) NOT NULL AUTO_INCREMENT,
    name varchar(128) NOT NULL,
    PRIMARY KEY (id),
    KEY name (name)
);
ALTER TABLE countries ADD UNIQUE KEY `country` (name);

CREATE TABLE cities (
    id int(11) NOT NULL AUTO_INCREMENT,
    name varchar(128) NOT NULL,
    country_name varchar(128) NOT NULL,
    PRIMARY KEY (id)
);
ALTER TABLE cities ADD UNIQUE KEY `city_country` (name, country_name);

CREATE TABLE universities (
    id int(11) NOT NULL AUTO_INCREMENT,
    name varchar(128) NOT NULL,
    city_id varchar(128) NOT NULL,
    PRIMARY KEY (id)
);
ALTER TABLE universities ADD UNIQUE KEY `name` (name, city_id);



