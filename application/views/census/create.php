<?php echo form_open('census/create');
//echo validation_errors('<span class="error">', '</span>');
?>
<div class="question">
  <h3>I. Quod est tibi nomen? </h3>
  <?php echo form_error('FName'); ?>
  <label for="FName">Praenomen</label>
  <input type="input" id="Praenomen" name="FName" value="<?php echo set_value('FName'); ?>"><br/>
</div>

<div class="question">
  <?php echo form_error('LName'); ?>
  <label for="LName">Nomen Gentilicium</label>
  <input type="input" id="Nomen" name="LName" value="<?php echo set_value('LName'); ?>"><br/>
</div>

<div class="question">
  <h3>II. Cuius generis es? Sum generis...</h3>
  <?php echo form_error('Gender'); ?>
  <select name="Gender" class="select" id="Gender" onchange="showOther('#Gender','#gender-other-textbox', 'OtherGender', '')">
            <option value="" <?php echo  set_select('Gender', '',TRUE); ?>>elige</option>
            <?php foreach ($genders as $gender) { ?>
              <?php if ($gender == 'alius') { ?>
                <option value='other' <?php echo  set_select('Gender', 'other'); ?>>
                <?php echo $gender; ?>
              <?php } else { ?>
                <option value="<?php echo $gender; ?>" <?php echo set_select('Gender', $gender); ?>>
                  <?php echo $gender; ?>
                </option>
              <?php } ?>
            <?php } ?>
        </select>
</div>

<!--<?php echo form_error('other-gender'); ?>-->
<div id='gender-other-textbox' name='gender-other-textbox' class="to-the-right">
</div>

<div class="question">
  <h3>III. Cuius hominum varietatis es?</h3>
  <?php echo form_error('Ethnicity'); ?>
  <select name="Ethnicity" class="select" id="ethnicities" onchange="showOther('#ethnicities','#ethnicities-other-textbox', 'OtherEthnicity', '')">
            <option value="" <?php echo set_select('Ethnicity','',TRUE); ?>>elige</option>
            <?php foreach ($ethnicities as $ethnic) { ?>
              <?php if ($ethnic == 'Alius') { ?>
                <option value='other' <?php echo  set_select('Ethnicity', 'other'); ?>>
                <?php echo $ethnic; ?>
              <?php } else { ?>
                <option value="<?php echo $ethnic; ?>" <?php echo set_select('Ethnicity', $ethnic); ?>>
                  <?php echo $ethnic; ?>
                </option>
              <?php } ?>
            <?php } ?>
        </select>
</div>

<!--<?php echo form_error('other-ethnicities'); ?>-->
<div id='ethnicities-other-textbox' class="to-the-right">
</div>

<div class="question">
  <h3>IV. Quo anno natus vel nata es?</h3>
  <?php echo form_error('Age'); ?>
  <select name="Age" class="select" id="Age">
            <option value="" <?php echo set_select('Age','',TRUE); ?>>elige</option>
            <?php foreach ($birthYear as $year) { ?>
                <option value="<?php echo $year;?>" <?php echo set_select('Age', $year); ?>>
                  <?php echo $year; ?>
                </option>
            <?php } ?>
        </select>
</div>

<div class="question">
  <h3>V. Unde es oriundus vel oriunda? E/ex...</h3>
  <?php echo form_error('ContinentOfBirthId'); ?>
  <select name="ContinentOfBirthId" class="select" id="continents" onchange="showCountries('no','#countries-area1','','continents','','BirthCountryId')">
            <option value="" <?php echo set_select('ContinentOfBirthId','',TRUE); ?>>elige</option>
            <?php foreach ($continents as $key => $value) { ?>
                  <option value="<?php echo $value['id'];?>" <?php echo set_select('ContinentOfBirthId', $value['id']); ?>>
                  <?php echo $value['name'];
                      echo "</option>";
                  }?>
        </select>
  <div class="question" id="countries-area1">
  </div>
</div>

<div class="question">
  <h3>VI. Qua in terra habitas? In...</h3>
  <?php echo form_error('ContinentOfResidenceId'); ?>
  <select name="ContinentOfResidenceId" class="select" id="liveContinent" onchange="showCountries('yes', '#countries-area2', '#cityOfResidence', 'liveContinent', 'CityOfResidenceId', 'CountryOfResidenceId', '')">
            <option value="" <?php echo set_select('ContinentOfResidenceId','',TRUE); ?>>elige</option>
            <?php foreach ($continents as $key => $value) { ?>
                  <option value="<?php echo $value['id'];?>" <?php echo set_select('ContinentOfResidenceId', $value['id']); ?>>
                      <?php echo $value['name'];
                        echo "</option>";
                  }
            ?>
        </select>
  <div class="question" id="countries-area2">
  </div>
</div>

<div class="question">
  <h3>VII. Qua in urbe habitas? Habito... (Elige responsum infra conscriptum aut scribe urbis nomen Latine vel sermone patrio)</h3>
  <?php echo form_error('CityOfResidenceId'); ?>
  <div id='liveCity'>
    <div id='cityOfResidence'>
    </div>
    <div id="otherLiveCity">
      <input type="input" id="otherCityOfResidence" name="otherCityOfResidence" value="<?php echo set_value('otherCityOfResidence'); ?>" style="position:relative;float:right;margin-right:50%;margin-top: -3.5em;"/>
    </div>
  </div>
</div>

<div class="question">
  <h3>VIII. Quo munere fungeris? (Elige responsum infra conscriptum aut scribe muneris nomen Latine vel sermone patrio. Rudem si acceperis indica atque porro scribe quod munus expleveris.) </h3>
  <?php echo form_error('Profession'); ?>
  <select name="Profession" class="select" id="Profession" onchange="showOther('#Profession','#professions-other-textbox', 'OtherProfession', '')">
        <option value="" <?php echo set_select('Profession','',TRUE); ?>>elige</option>
        <?php foreach ($professions as $job) { ?>
            <option value="<?php echo $job; ?>" <?php echo set_select('Profession', $job); ?>>
              <?php echo $job; ?>
            </option>
        <?php } ?>
        <option value='other' <?php echo  set_select('Profession', 'other'); ?>>alio munere</option>
  </select>

  <div id='professions-other-textbox' style="position:relative;float:right;margin-right:50%;">
  </div>

  <div id="professions-retired-textbox" style="position:relative;float:right;margin-right:50%;">
    <input id="RetiredProfession" name="RetiredProfession" class="same-row-one-column-to-the-right">
  </div>
</div>
<div class="question">
  <h3>IX.  Quam fidem profiteris? (Elige responsum infra conscriptum aut scribe fidei nomen Latine vel sermone patrio). </h3>
  <?php echo form_error('Religion'); ?>
  <select name="Religion" class="select" id="religion" onchange="showOther('#religion','#religions-other-textbox', 'OtherReligion', '')">
      <option value="" <?php echo set_select('Religion','',TRUE); ?> >elige</option>
      <?php
      foreach ($religions as $religion) {
          if ($religion == 'fidem Christianam [Catholicam, Lutheranam, Calvinianam, aliam] profiteor') {
              $description = 'fidem Christianam profiteor';
              $religion = 'christian';
              $endOption = $description."</option>";
          } else {
              $endOption = $religion."</option>";
          }
          $option = "<option value='".$religion."'" . set_select('Religion', $religion) . ">";
          echo "$option $endOption";
      }
      ?>

        <option value='other' <?php echo set_select('Religion', 'other');?>>Aliam fidem profiteor</option>
        </select>

        <div id='christian-religions' style="margin-top:1em;position:relative;display:inline;">
	  <input type="radio" name="JCBelief" value="Catholic">Catholicam
	  <input type="radio" name="JCBelief" value="Lutheran">Lutheranam
	  <input type="radio" name="JCBelief" value="Calvinist">Calvinianam
	  <input type="radio" name="JCBelief" value="Christian-Other"> aliam
        </div>

        <div id='religions-other-textbox' style="position:relative;display:inline;">
        </div>
</div>
<div class="question">
  <h3>X. Quas linguas praeter Latinam didicisti? </h3>
  <?php echo form_error('Language'); ?>
  <div class="widget" style="width: 60%">
    <?php $i = 0;
          //echo "<fieldset>";
          foreach ($languages as $language) {
              ++$i;
              list($lang, $theRest) = preg_split("/[(]+/", $language);
              $theRest = str_replace('(','',$theRest);
              $theRest = str_replace(')','',$theRest);
              $langs = preg_split("/[,]+/", $theRest);
              $radio1LabelText = $langs[0];
              $radio2LabelText = $langs[1];
              $radio3LabelText = $langs[2];
              $radio4LabelText = $langs[3];

              $checkboxLabel = "<label for='checkbox-".$i."'>".$lang."</label>";
              $checkboxHtml = "<input type='checkbox' class='fieldsetbox' name='checkbox-".$i."' value='".$lang."'>";
              $radio1label = "<label for='radio-" . $i . "'>" . $radio1LabelText . "</label>";
              $radio1Html = "<input type='radio' name='radio-" . $i . "' value='" . $radio1LabelText . "'>";
              $radio2label = "<label for='radio-" . $i . "'>" . $radio2LabelText . "</label>";
              $radio2Html = "<input type='radio' name='radio-" . $i . "' value='" . $radio2LabelText . "'>";
              $radio3label = "<label for='radio-" . $i . "'>" . $radio3LabelText . "</label>";
              $radio3Html = "<input type='radio' name='radio-" . $i . "' value='" . $radio3LabelText . "'>";
              $radio4label = "<label for='radio-" . $i . "'>" . $radio4LabelText . "</label>";
              $radio4Html = "<input type='radio' name='radio-" . $i . "' value='" . $radio4LabelText . "'>";
              $fieldset = "<fieldset name='set-" . "$i" . "'>";

              echo "$checkboxLabel $checkboxHtml $fieldset $radio1label $radio1Html $radio2label $radio2Html $radio3label $radio3Html $radio4label $radio4Html
              </fieldset><br>";
            }
        ?>
    <br>Aliam: <input type='text' name='other-text-box-X'>
  </div>
</div>

<div class="question">
  <h3>XI. Quot annos natus Latine discere coepisti?</h3>
  <?php echo form_error('AgeStart'); ?>
  <select name="AgeStart" class="select" id="ageStarted">
            <option selected='selected' value="" <?php echo set_value('AgeStart','',TRUE); ?>>elige</option>
            <?php foreach ($ageStarted as $age) { ?>
                <option value="<?php echo $age; ?>" <?php echo set_select('AgeStart', $age); ?>>
                  <?php echo $age; ?>
                </option>
            <?php } ?>
        </select>
</div>

<div class="question">
  <h3>XII. Quomodo Latine discere coepisti? </h3>
  <?php echo form_error('HowLearn'); ?>
  <select name="HowLearn" class="select" id="howLearned" onchange="showOther('#howLearned','#howLearned-other-textbox', 'OtherHowLearn', '')">
            <option selected='selected' value="" <?php echo set_value('HowLearn','',TRUE); ?>>elige</option>
            <?php foreach ($howLearned as $learner) { ?>
                <option value="<?php echo $learner; ?>" <?php echo set_select('HowLearn', $learner); ?>>
                  <?php echo $learner; ?>
                </option>
            <?php } ?>
            <option value='other' <?php echo set_select('HowLearn', 'other'); ?>>alia ratione</option>
        </select>
  <!--<?php echo form_error('howLearned-other-textbox'); ?>-->
  <div id='howLearned-other-textbox' name='howLearned-other-textbox' style="position:relative;float:right;margin-right:50%;">
  </div>
</div>

<div class="question">
  <h3>XIII. Qua ratione sermone Latino uteris? </h3>
  <?php echo form_error('HowUse'); ?>
  <ul style="list-style-type: none;">
    <?php
          $i = 0;
          foreach ($howUse as $how) {
            ++$i;
            $checkboxLabel = "<label for='how-checkbox-".$i."'>".$how."</label>";
            $checkboxHtml = "<input type='checkbox' name='how-checkbox-".$i."' value='".$how."' <?php echo set_select('HowUse', $how); ?>>";
            echo "<li style='float: left;padding: 10px;justify-content: left;'>";
            echo $checkboxLabel;
            echo $checkboxHtml;
            echo "</li>";
          }?>
  </ul>
</div>

<div class="question"><br><br>
  <h3>XIV. Quamdiu iam sermone Latino uteris?  </h3>
<table>
<tr>
<td>
legendo
    <select name="YearsRead">
<option value='0'> Numquam</option>
<option value='1'>1 Mensem</option>
<option value='2'>2 Menses</option>
<option value='3'>3 Menses</option>
<option value='4'>4 Menses</option>
<option value='5'>5 Menses</option>
<option value='6'>6 Menses</option>
<option value='7'>7 Menses</option>
<option value='8'>8 Menses</option>
<option value='9'>9 Menses</option>
<option value='10'>10 Menses</option>
<option value='11'>11 Menses</option>
<option value='12'> 1 Annum</option>
<option value='24'>2 Annos</option>
<option value='36'>3 Annos</option>
<option value='48'>4 Annos</option>
<option value='60'>5 Annos</option>
<option value='72'>6 Annos</option>
<option value='84'>7 Annos</option>
<option value='96'>8 Annos</option>
<option value='108'>9 Annos</option>
<option value='120'>10 Annos</option>
<option value='132'>11 Annos</option>
<option value='144'>12 Annos</option>
<option value='156'>13 Annos</option>
<option value='168'>14 Annos</option>
<option value='180'>15 Annos</option>
<option value='192'>16 Annos</option>
<option value='204'>17 Annos</option>
<option value='216'>18 Annos</option>
<option value='228'>19 Annos</option>
<option value='240'>20 Annos</option>
<option value='252'>21 Annos</option>
<option value='264'>22 Annos</option>
<option value='276'>23 Annos</option>
<option value='288'>24 Annos</option>
<option value='300'>25 Annos</option>
<option value='312'>26 Annos</option>
<option value='324'>27 Annos</option>
<option value='336'>28 Annos</option>
<option value='348'>29 Annos</option>
<option value='360'>30 Annos</option>
<option value='372'>31 Annos</option>
<option value='384'>32 Annos</option>
<option value='396'>33 Annos</option>
<option value='408'>34 Annos</option>
<option value='420'>35 Annos</option>
<option value='432'>36 Annos</option>
<option value='444'>37 Annos</option>
<option value='456'>38 Annos</option>
<option value='468'>39 Annos</option>
<option value='480'>40 Annos</option>
<option value='53'>plusquam 40 annos</option>
</select>
</td>
<td>
loquendo
    <select name="YearsSpeak">
<option value='0'> Numquam</option>
<option value='1'>1 Mensem</option>
<option value='2'>2 Menses</option>
<option value='3'>3 Menses</option>
<option value='4'>4 Menses</option>
<option value='5'>5 Menses</option>
<option value='6'>6 Menses</option>
<option value='7'>7 Menses</option>
<option value='8'>8 Menses</option>
<option value='9'>9 Menses</option>
<option value='10'>10 Menses</option>
<option value='11'>11 Menses</option>
<option value='12'> 1 Annum</option>
<option value='24'>2 Annos</option>
<option value='36'>3 Annos</option>
<option value='48'>4 Annos</option>
<option value='60'>5 Annos</option>
<option value='72'>6 Annos</option>
<option value='84'>7 Annos</option>
<option value='96'>8 Annos</option>
<option value='108'>9 Annos</option>
<option value='120'>10 Annos</option>
<option value='132'>11 Annos</option>
<option value='144'>12 Annos</option>
<option value='156'>13 Annos</option>
<option value='168'>14 Annos</option>
<option value='180'>15 Annos</option>
<option value='192'>16 Annos</option>
<option value='204'>17 Annos</option>
<option value='216'>18 Annos</option>
<option value='228'>19 Annos</option>
<option value='240'>20 Annos</option>
<option value='252'>21 Annos</option>
<option value='264'>22 Annos</option>
<option value='276'>23 Annos</option>
<option value='288'>24 Annos</option>
<option value='300'>25 Annos</option>
<option value='312'>26 Annos</option>
<option value='324'>27 Annos</option>
<option value='336'>28 Annos</option>
<option value='348'>29 Annos</option>
<option value='360'>30 Annos</option>
<option value='372'>31 Annos</option>
<option value='384'>32 Annos</option>
<option value='396'>33 Annos</option>
<option value='408'>34 Annos</option>
<option value='420'>35 Annos</option>
<option value='432'>36 Annos</option>
<option value='444'>37 Annos</option>
<option value='456'>38 Annos</option>
<option value='468'>39 Annos</option>
<option value='480'>40 Annos</option>
<option value='53'>plusquam 40 annos</option>
</select>
</td>
<td>
scribendo
    <select name="YearsWrite">
<option value='0'> Numquam</option>
<option value='1'>1 Mensem</option>
<option value='2'>2 Menses</option>
<option value='3'>3 Menses</option>
<option value='4'>4 Menses</option>
<option value='5'>5 Menses</option>
<option value='6'>6 Menses</option>
<option value='7'>7 Menses</option>
<option value='8'>8 Menses</option>
<option value='9'>9 Menses</option>
<option value='10'>10 Menses</option>
<option value='11'>11 Menses</option>
<option value='12'> 1 Annum</option>
<option value='24'>2 Annos</option>
<option value='36'>3 Annos</option>
<option value='48'>4 Annos</option>
<option value='60'>5 Annos</option>
<option value='72'>6 Annos</option>
<option value='84'>7 Annos</option>
<option value='96'>8 Annos</option>
<option value='108'>9 Annos</option>
<option value='120'>10 Annos</option>
<option value='132'>11 Annos</option>
<option value='144'>12 Annos</option>
<option value='156'>13 Annos</option>
<option value='168'>14 Annos</option>
<option value='180'>15 Annos</option>
<option value='192'>16 Annos</option>
<option value='204'>17 Annos</option>
<option value='216'>18 Annos</option>
<option value='228'>19 Annos</option>
<option value='240'>20 Annos</option>
<option value='252'>21 Annos</option>
<option value='264'>22 Annos</option>
<option value='276'>23 Annos</option>
<option value='288'>24 Annos</option>
<option value='300'>25 Annos</option>
<option value='312'>26 Annos</option>
<option value='324'>27 Annos</option>
<option value='336'>28 Annos</option>
<option value='348'>29 Annos</option>
<option value='360'>30 Annos</option>
<option value='372'>31 Annos</option>
<option value='384'>32 Annos</option>
<option value='396'>33 Annos</option>
<option value='408'>34 Annos</option>
<option value='420'>35 Annos</option>
<option value='432'>36 Annos</option>
<option value='444'>37 Annos</option>
<option value='456'>38 Annos</option>
<option value='468'>39 Annos</option>
<option value='480'>40 Annos</option>
<option value='53'>plusquam 40 annos</option>
</select>
</td>
<td>
auscultando
    <select name="YearsListen">
<option value='0'> Numquam</option>
<option value='1'>1 Mensem</option>
<option value='2'>2 Menses</option>
<option value='3'>3 Menses</option>
<option value='4'>4 Menses</option>
<option value='5'>5 Menses</option>
<option value='6'>6 Menses</option>
<option value='7'>7 Menses</option>
<option value='8'>8 Menses</option>
<option value='9'>9 Menses</option>
<option value='10'>10 Menses</option>
<option value='11'>11 Menses</option>
<option value='12'> 1 Annum</option>
<option value='24'>2 Annos</option>
<option value='36'>3 Annos</option>
<option value='48'>4 Annos</option>
<option value='60'>5 Annos</option>
<option value='72'>6 Annos</option>
<option value='84'>7 Annos</option>
<option value='96'>8 Annos</option>
<option value='108'>9 Annos</option>
<option value='120'>10 Annos</option>
<option value='132'>11 Annos</option>
<option value='144'>12 Annos</option>
<option value='156'>13 Annos</option>
<option value='168'>14 Annos</option>
<option value='180'>15 Annos</option>
<option value='192'>16 Annos</option>
<option value='204'>17 Annos</option>
<option value='216'>18 Annos</option>
<option value='228'>19 Annos</option>
<option value='240'>20 Annos</option>
<option value='252'>21 Annos</option>
<option value='264'>22 Annos</option>
<option value='276'>23 Annos</option>
<option value='288'>24 Annos</option>
<option value='300'>25 Annos</option>
<option value='312'>26 Annos</option>
<option value='324'>27 Annos</option>
<option value='336'>28 Annos</option>
<option value='348'>29 Annos</option>
<option value='360'>30 Annos</option>
<option value='372'>31 Annos</option>
<option value='384'>32 Annos</option>
<option value='396'>33 Annos</option>
<option value='408'>34 Annos</option>
<option value='420'>35 Annos</option>
<option value='432'>36 Annos</option>
<option value='444'>37 Annos</option>
<option value='456'>38 Annos</option>
<option value='468'>39 Annos</option>
<option value='480'>40 Annos</option>
<option value='53'>plusquam 40 annos</option>
</select>
</td>
</tr>
</table>
</div>

<div class="question">
  <h3>XV. Si qua in institutione (in schola inferiori vel in universitate studiorum vel in cursu per rete) in sermonem Latinum incubuisti, elige eius nomen infra conscriptum aut scribe nomen Latine vel sermone patrio: </h3>
  <?php echo form_error('schoolContinent'); ?>
  <select name="schoolContinent" class="select" id="schoolContinent" onchange="showUniversityCountries('no', '#countries-XV', '#cities-XV', 'schoolContinent', 'univ-cities-select', 'univContinent')">
      <option selected='selected' value="" <?php echo set_value('schoolContinent','',TRUE); ?>>elige</option>
      <?php foreach ($continents as $key => $value) { ?>
            <option value="<?php echo $value['id'];?>" <?php echo  set_select('schoolContinent', $value['id']); ?>>
            <?php echo $value['name'];
                  echo "</option>";
            }
      ?>
      <option value="other">Cursus per rete omnium gentium</option>
  </select>

  <div class="other" id="online-course">
    <label for="OnlineCourse">Scribe nomen:</label>
    <input type="input" id="OnlineCourse" name="onlineCourse" /><br/>
  </div>

  <div class="select" id="countries-XV">
  </div>

  <div class="select" id="cities-XV">
  </div>

  <div class="select" id='universities' name='universities'>
  </div>

  <div class="textbox" id='other-school-XV'>
    <!--<?php echo form_error('otherSchool'); ?>-->
    <label for="otherSchool">Alii Officium?</label>
    <input type="input" id="otherSchool" name="otherSchool" /><br/>
  </div>
</div>

<div class="question">
  <h3>XVI. Si cui conventui Latino (aestivo tempore aut alias) interfuisti, elige responsum infra conscriptum aut scribe conventus nomen Latine vel sermone patrio. Conventum petivi q.i.: </h3>
  <?php echo form_error('SeminarsAttended'); ?>
  <div class="widget" style="width: 60%">
    <?php $i = 0;
          //echo "<fieldset>";
          foreach ($seminars as $seminar) {
              ++$i;
              list($sem, $theRest) = preg_split("/[(]+/", $seminar);
              $theRest = str_replace('(','',$theRest);
              $theRest = str_replace(')','',$theRest);
              $sems = preg_split("/[,]+/", $theRest);
              $radio1LabelText = $sems[0];
              $radio2LabelText = $sems[1];
              $radio3LabelText = $sems[2];
              $radio4LabelText = $sems[3];

              $checkboxLabel = "<label for='seminar-checkbox-".$i."'>".$sem."</label>";
              $checkboxHtml = "<input type='checkbox' class='fieldsetbox' name='seminar-checkbox-".$i."' value='".$sem."'>";
              $radio1label = "<label for='seminar-radio-" . $i . "'>" . $radio1LabelText . "</label>";
              $radio1Html = "<input type='radio' name='seminar-radio-" . $i . "' value='" . $radio1LabelText . "'>";
              $radio2label = "<label for='seminar-radio-" . $i . "'>" . $radio2LabelText . "</label>";
              $radio2Html = "<input type='radio' name='seminar-radio-" . $i . "' value='" . $radio2LabelText . "'>";
              $radio3label = "<label for='seminar-radio-" . $i . "'>" . $radio3LabelText . "</label>";
              $radio3Html = "<input type='radio' name='seminar-radio-" . $i . "' value='" . $radio3LabelText . "'>";
              $radio4label = "<label for='seminar-radio-" . $i . "'>" . $radio4LabelText . "</label>";
              $radio4Html = "<input type='radio' name='seminar-radio-" . $i . "' value='" . $radio4LabelText . "'>";
              $fieldset = "<fieldset name='set-" . "$i" . "'>";

              echo "$checkboxLabel $checkboxHtml $fieldset $radio1label $radio1Html $radio2label $radio2Html $radio3label $radio3Html $radio4label $radio4Html
              </fieldset><br>";
            }
        ?>
    <br>Scribe nomen: <input type='text' name='other-seminars-text-box'>
  </div>
</div>

<div class="question">
  <h3>XVII. Si quo libro scholari usus vel usa es, elige nomen infra conscriptium vel eius nomen scribe Latine vel sermone patrio: </h3>
  <?php echo form_error('Textbook'); ?>
  <select name="Textbook" class="select" id="textbook" onchange="showOther('#textbook','#textbook-other-textbox','OtherTextbook','')">
            <option selected='selected' value="" <?php echo set_value('Textbook','',TRUE); ?>>elige</option>
            <?php foreach ($textbooks as $textbook) { ?>
                <option value="<?php echo $textbook; ?>" <?php echo set_select('Textbook', $textbook); ?>>
                  <?php echo $textbook; ?>
                </option>
            <?php } ?>
            <option value='other' <?php echo set_select('Textbook', 'other'); ?>>Scribe nomen:</option>
        </select>
  <!--<?php echo form_error('textbook-other-textbox'); ?>-->
  <div id='textbook-other-textbox' name='textbook-other-textbox' style="position:relative;float:right;margin-right:50%;">
  </div>
</div>

<div class="question">
  <h3>XVIII. Ubi sermone Latino uti soles? Elige omnes rationes quibus uteris vel scribe Latine vel sermone patrio: </h3>
  <div class="checkbox-group">
    <input type="checkbox" name="Use1" value="in schola inferiori" <?php echo set_checkbox( 'Use1', 'in schola inferiori'); ?>>in schola inferiori<br>
    <input type="checkbox" name="Use2" value="in universitate studiorum" <?php echo set_checkbox( 'Use2', 'in universitate studiorum'); ?>>in universitate studiorum<br>
    <input type="checkbox" name="Use3" value="in seminariis Latinis aestivis" <?php echo set_checkbox( 'Use3', 'in seminariis Latinis aestivis'); ?>>in seminariis Latinis aestivis<br>
    <input type="checkbox" name="Use4" value="in seminariis Latinis quae aliis temporibus fiunt" <?php echo set_checkbox( 'Use4', 'in seminariis Latinis quae aliis temporibus fiunt'); ?>>in seminariis Latinis quae aliis temporibus fiunt<br>
    <input type="checkbox" name="Use5" value="in circuli Latini sessionibus" <?php echo set_checkbox( 'Use5', 'in circuli Latini sessionibus'); ?>>in circuli Latini sessionibus<br>
    <input type="checkbox" name="Use6" value="in pagina prosopographica retis omnium gentium" <?php echo set_checkbox( 'Use6', 'in pagina prosopographica retis omnium gentium'); ?>>in pagina prosopographica retis omnium gentium<br>
    <input type="checkbox" name="Use7" value="auscultando nuntios Latinos retis omnium gentium" <?php echo set_checkbox( 'Use7', 'auscultando nuntios Latinos retis omnium gentium'); ?>>auscultando nuntios Latinos retis omnium gentium<br>
    <input type="checkbox" name="Use8" value="auscultando acroamata, quae vulgo PODCASTS dicuntur" <?php echo set_checkbox( 'Use8', 'auscultando acroamata, quae vulgo PODCASTS dicuntur'); ?>>auscultando acroamata, quae vulgo PODCASTS dicuntur<br>
    <input type="checkbox" name="Use9" value="legendo libros Latinos" <?php echo set_checkbox( 'Use9', 'legendo libros Latinos'); ?>>legendo libros Latinos<br>
    <input type="checkbox" name="Use10" value="egendo nuntios Latinos retis omnium gentium" <?php echo set_checkbox( 'Use10', 'legendo nuntios Latinos retis omnium gentium'); ?>>legendo nuntios Latinos retis omnium gentium<br>
    <input type="checkbox" name="Use11" value="scribendo epistulas Latinas ad amicos" <?php echo set_checkbox( 'Use11', 'scribendo epistulas Latinas ad amicos'); ?>>scribendo epistulas Latinas ad amicos<br>
    <input type="checkbox" name="Use12" value="scribendo libellos Latinos" <?php echo set_checkbox( 'Use12', 'scribendo libellos Latinos'); ?>>scribendo libellos Latinos<br>
    <input type="checkbox" name="Use13" value="loquendo cum amicis per Skype vel hoc genus instrumenti" <?php echo set_checkbox( 'Use13', 'loquendo cum amicis per Skype vel hoc genus instrumenti'); ?>>loquendo cum amicis per Skype vel hoc genus instrumenti<br>
  </div>
  <div id='use-other-textbox'>
    <!--<?php echo form_error('otherUse'); ?>-->
    <label for="otherSchool">Alia ratione:</label>
    <input type="input" id="otherUse" name="otherUse" /><br/>
  </div>
</div>

<div class="question">
  <?php echo form_error('EmailAddress'); ?>
  <label for="email"><h3>XIX. Quae est inscriptio tua electronica?</h3></label>
  <input type="input" id="email" name="EmailAddress" value="<?php echo set_value('EmailAddress', ''); ?>" /><br/>
</div>

<div class="comment">
  <?php echo form_error('comment'); ?>
  <label for="comment">
            <h3>XX. Si quid hoc de censu vel de Latini sermonis usu dicere velis, infra scribe:</h3></label> (Responsum non excedat numerum D vocabulorum!)
  <textarea rows="7" cols="71" id="comment" name="comment" value="<?php echo set_value('comment', ''); ?>"></textarea><br/>
</div>
<center>
  <input type="submit" name="submit" value="Mitte indicia tua ad censum!" />
</center>
</form>
