<div>
    <div class='indentout'>
      <p class='wide'>

        <strong>EXSPECTATVS</strong>
          ad primum Censum Latinum omnium hominum quavis e gente oriundorum accessisti. Census ab Universitatis studiorum Vasintoniae Occidentalis (WWU) professoribus institutus est, auspiciis Academiae Latinitati Fovendae (ALF) sodalium, duobus praesertim propositis:
      </p>
      <div class="indent">
        <ul>
          <li>
            Ut homines, qui nostra aetate sermone Latino utuntur, numerentur;
          </li>
          <li>
            Quo melius Latini sermonis usus atque condiciones, quae nostra aetate sunt, intelligantur.
          </li>
        </ul>
      </div>
      <p class="wide">
        Huius census indiciis decursu annorum descriptis, rite ordinatis, inter se comparatis demonstrari poterit utrum sermonis usus increbrescat necne, quibus in orbis terrarum regionibus sermo Latinus maxime usurpetur, qui libri ad eius studium saepius adhibeantur et alia.
      </p>
      <p class="wide">
        Census Latinus quinque quoque anno ab eisdem professoribus instituetur, et indicia in eo collecta in publicum edentur. Ab illis, qui respondere velint, nihil poscetur, quod ad vitam privatam pertineat. Talia enim nullo iure in rete universali exhibentur.
      </p>
      <center>
        <p class="widefin">
          <strong>PERGRATVM</strong>
          feceris, si Censui Latino anni MMXVIII interfueris. Quod X minuta ad summum requiret.
        </p>
        <!--
        <a href="http://localhost:8080/index.php/pages/view/census">Nomen tuum da ad CENSVM LATINVM anni MMXVIII!</a>
      -->
        <a href="index.php/census/create">Nomen tuum da ad CENSVM LATINVM anni MMXVIII!</a>
      </center>
    </div>
</div>
