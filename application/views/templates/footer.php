    <div align='center'>
      <hr width='100%'/>
      <br/>
      <br/>
      <em>&copy; 2018</em>
    </div>
    <script>
        $( function() {
            $("#other-school-XV").css("display", "none");
            $(".widget fieldset").css("display", "none");
            $("#otherLiveCity").css("display", "none");
            $("#christian-religions").css("display", "none");
            $("#online-course").css("display", "none");
            $("#professions-other-textbox").css("display", "none");
            $("#professions-retired-textbox").css("display", "none");
        });

        $(document).on('change', function(){
          if ($('#univ-cities-select').val() == "other"){
              $("#other-school-XV").css("display", "block");
          } else {
              $("#other-school-XV").css("display", "none");
          }

          if ($('#schoolContinent').val() == "other"){
              $("#online-course").css("display", "block");
          } else {
              $("#online-course").css("display", "none");
              $("#countries-XV").css("display", "block");
          }

          if ($('#CityOfResidenceId').val() === "other"){
              $("#otherLiveCity").css("display", "block");
          } else {
              $("#otherLiveCity").css("display", "none");
          }

          if ($('#Profession').val() === "rude donatus vel donata. Eram"){
              $("#professions-retired-textbox").css("display", "block");
          } else {
              $("#professions-retired-textbox").css("display", "none");
          }

          if ($('#Profession').val() == "other"){
              $("#professions-other-textbox").css("display", "block");
          } else {
              $("#professions-other-textbox").css("display", "none");
          }

          if ($('#religion').val() === "christian"){
              $("#christian-religions").css("display", "block");
          } else {
              $("#christian-religions").css("display", "none");
          }
    
        });

        $(".fieldsetbox").on("change", () => {
          //loop through all check boxes
            //=> check if their fieldset is active
              //if not, pass this checkbox to showFieldsets
          for(var i = 0; i < $(".fieldsetbox").length; i++)
            if($(".fieldsetbox")[i].checked){
              if($(".fieldsetbox").eq(i).next("fieldset").css("display") == "none")
                $(".fieldsetbox").eq(i).next("fieldset").css("display", "block");
            }else
              $(".fieldsetbox").eq(i).next("fieldset").css("display", "none");
        });

        /* This function is called when a continents select list gets changed.  It's job is to
        *  pass enough info to the callback function so that it can go to the db and Populate
        *  a countries dropdown.  If it's called with citiesToo set to 'yes' then the country
        *  dropdown's options will have an onchange(showCites()) event handler that will render
        *  a dropdown of cities for the country selected.
        *
        *  The following info needs to be sent to this func so it can do it's job:
        *
        *  Param rundown:
        *  citiesToo yes|no  If yes then include an 'onchange(showCites)'. Set to 'no' if
        *                     you don't need a cities dropdown created after the countries dropdown.
        *  countriesAreaId   This is the id of the div that you want the results rendered in.
        *  citiesAreaId      This is the id of the div area you want the cities renderend in.
        *  continentsSelectId  This is the id of the dropdown that holds the continents.
        *  citiesSelectId    This is the id that you want the cities dropdown to have.  Don't set if
        *                    not interested in a cities dropdown.
        *  countriesSelectId This is the id you want the countries dropdown id to be
        *  continentId       This should not be passed, it is set below.  Just leave an empty ''.
        */
        function showCountries(citiesToo, countriesAreaId, citiesAreaId, continentsSelectId,citiesSelectId,countriesSelectId,continentId) {
            var dataObj =
             {citiesToo:citiesToo,
              citiesSelectId:citiesSelectId,
              citiesAreaId:citiesAreaId,
              countriesAreaId:countriesAreaId,
              countriesSelectId:countriesSelectId,
              continentsSelectId:continentsSelectId,
              continentId:document.getElementById(continentsSelectId).value};
            /*
            alert('The args are: citiesToo = ' + citiesToo + '\n' +
                  '              countriesAreaId = ' + countriesAreaId + '\n' +
                  '              citiesAreaId = ' + citiesAreaId + '\n' +
                  '              continentsSelectId = ' + continentsSelectId + '\n' +
                  '              citiesSelectId = ' + citiesSelectId + '\n' +
                  '              countriesSelectId = ' + countriesSelectId + '\n' +
                  '              continentId = ' + continentId + '\n');
            */
            if (! continentsSelectId) { alert('Bad call to showCountries().  Missing continent select id'); }
            if (! countriesAreaId) { alert('Bad call to showCountries().  No display area.'); }
            if (citiesToo == 'yes' && (! countriesSelectId || ! citiesSelectId || ! citiesAreaId)) {
                alert('Bad call to showCountries().  Not enough info to create cities list.');
            }
            $.ajax({
                url: 'index.php/census/getCountries',
                data: dataObj,
                //processData: false,
                dataType: "html",
                cache: false,
                success: function(r,s,t) {
                    $(countriesAreaId).html(t.responseText);
                }
            });
        }

        /* This function is called when you need all the countries with latin universities associated
        *  with a particular continent.  If it's
        *  called with citiesToo set to 'yes' then the country
        *  dropdown's options will have an onchange(showUniversityCites()) event handler that will render
        *  a dropdown of cities for the country selected.
        *
        *  The following info needs to be sent to this func so it can do it's job:
        *
        *  Param rundown:
        *  citiesToo yes|no     If yes then include an 'onchange(showUniversityCities)'. Set to 'no' if
        *                         you don't need a cities dropdown created after the countries dropdown.
        *  countriesAreaId      This is the id of the div that you want the results rendered in.
        *  countriesSelectId    This is the id that you want the countries dropdown to have.
        *  continentsSelectId   This is the id of the dropdown that holds the continents.
        *  continentId          This is the id of the selected continent.
        *  citiesAreaId         This is the id of the div area you want the cities renderend in.
        *  citiesSelectId       This is the id that you want the cities dropdown to have.  Don't set
        *                       not interested in a cities dropdown.
        */

        function showUniversityCountries(citiesToo, countriesAreaId, citiesAreaId,
                                         continentsSelectId,citiesSelectId,countriesSelectId,continentId) {
            var dataObj =
             {citiesToo:citiesToo,
              citiesSelectId:citiesSelectId,
              citiesAreaId:citiesAreaId,
              countriesAreaId:countriesAreaId,
              countriesSelectId:countriesSelectId,
              continentsSelectId:continentsSelectId,
              continentId:document.getElementById(continentsSelectId).value};
            var funcName = "showUniversityCountries";
            if (! continentsSelectId) { alert('Bad call to showCountries().  Missing continent select id'); }
            if (! countriesAreaId) { alert('Bad call to showCountries().  No display area.'); }
            if (citiesToo == 'yes' && (! countriesSelectId || ! citiesSelectId || ! citiesAreaId)) {
                alert('Bad call to showCountries().  Not enough info to create cities list.');
            }

            if (document.getElementById('schoolContinent').value == 'other') {
                return;
            } 

            $.ajax({
                url: 'index.php/census/getUniversityCountries',
                data: dataObj,
                //processData: false,
                dataType: "html",
                cache: false,
                success: function(r,s,t) {
                    $(countriesAreaId).html(t.responseText);
                }
            });
        }

        /* Args:
        Use countriesSelectId to get id of country from which to find cities.
        Use citiesAreaId as the area to populate with a list of cities.
        Use citiesSelectId as the id of the select list that will be created.
        */
        function showCities(countriesSelectId, citiesSelectId, citiesAreaId){
            var dataObj =
             {citiesSelectId:citiesSelectId,
              citiesAreaId:citiesAreaId,
              countriesSelectId:countriesSelectId,
              countryId:document.getElementById(countriesSelectId).value};
              $.ajax({
                  url: 'index.php/census/getCities',
                  data: dataObj,
                  dataType: "html",
                  cache: false,
                  success: function(r,s,t) {
                      $(citiesAreaId).html(t.responseText);
                  },
                  fail: function(r,s,t) {
                      alert('showCities:  Call to census/getCities() failed miserably...');
                  }
              });
        }

        //showUniversitiesByCountry('univContinent','univ-cities-select','#cities-XV')
        function showUniversitiesByCountry(countriesSelectId, citiesSelectId, citiesAreaId){
          var dataObj =
           {citiesSelectId:citiesSelectId,
            citiesAreaId:citiesAreaId,
            countriesSelectId:countriesSelectId,
            countryId:document.getElementById(countriesSelectId).value};
          $.ajax({
              url: 'index.php/census/getUniversitiesByCountry',
              data: dataObj,
              dataType: "html",
              cache: false,
              success: function(r,s,t) {
                  $(citiesAreaId).html(t.responseText);
              },
              fail: function(r,s,t) {
                  alert('showCities:  You failed...');
              }
          });
        }

        function showOther(selectId, otherAreaId, textboxId, labelText) {
            // Args:
            // selectId:    The id of the select control who's value may or may not be 'other'
            // otherAreaId: The "div" area within which to display the text box.
            // textboxId:   The id of the textbox that will be created.
            // labelText:   The text for the label of the textbox that will be created.
            if ($(selectId).val() == 'other') {
                var responseText = "<label for='" + textboxId + "'>" + labelText + "</label>";
                responseText += "<input id='" + textboxId + "' name='" + textboxId + "' class='same-row-one-column-to-the-right'>";
                responseText += "</input>";
                $(otherAreaId).html(responseText);
                document.getElementById(textboxId).focus();
            } else {
                $(otherAreaId).html('');
            }
        }
    </script>
  </body>
</html>
