- Under this dir you'll find scripts, sql and data files:
  - bin: This directory has scripts.
    - latinCensusData.pl:  This script loads data from the data files located under
      data/files into the latincensus database (which needs to already exist).  DB config info
      can be found at the top of the script.
      The following tables get populated:
      - countries
      - cities
      - universities
      The table "continents" was populated manually with the following continents:
      - Africa
      - Asia
      - Europa
      - America Septentrionali
      - America Meridiana
  - data:
    - ddl:  The sql to rebuild the schema for the read-only data.
    - dml:  The sql to populate the read only data.  If this directory is empty it's because
            the data is being loaded by the latinCensusData.pl script based on the data
            files under data/files.
    - files:This is where the data files are.

  To populate the static, read-only data you should do this:
  - Create a database named latincensus.
  - Create a db user named census indentified by census.
  - Grant the db user privs:
    grant all privileges on latincensus.* to 'census'@'localhost' identified by 'census';
  - Run the ddl in data/ddl dir.
  - Run the latinCensusData.pl script located in the bin directory.
