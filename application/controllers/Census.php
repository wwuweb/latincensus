<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Census extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('census_model');
        $this->load->helper('url_helper');
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->init();
    }

    public function index($page = null) {
        $data['users'] = $this->census_model->get_users();
        if (empty($data['users']))
        {
            show_404();
        }
        $data['title'] = 'CENSVS LATINVS';
        $this->load->view('templates/header', $data);
        $this->load->view('census/index', $data);
        $this->load->view('templates/footer', $data);
    }

    public function view($id = 0) {
        $data['user'] = $this->census_model->get_user($id);

        if (empty($data['users']))
        {
            show_404();
        }

        $data['title'] = 'CENSVS LATINVS';
        $this->load->view('templates/header', $data);
        $this->load->view('census/view', $data);
        $this->load->view('templates/footer');
    }

    public function create() {
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->setValidation();

        if ($this->form_validation->run() === FALSE) {
            $this->load->view('templates/header', $this->session->userdata);
            $this->load->view('census/create', $this->session->userdata);
            $this->load->view('templates/footer', $this->session->userdata);
        } else {
            $this->census_model->save();
            $this->load->view('census/success');
        }
    }

    function init() {
        // Populate our static data structs (things like genders, etc)
        $staticData = array(
            'title'    => 'CENSVS LATINVS',
            'genders'  => array('feminini', 'masculini', 'alius'
            ),
            'ethnicities' => array('Aethiopicae',
                                   'Caucasicae',
                                   'Mongolicae',
                                   'Americanae Septentrionalis nativae',
                                   'Americanae Meridianae nativae',
                                   'Australianae nativae',
                                   'Mixtae',
                                   'Alius'
            ),
            'birthYear' => $this->getYears(),
            'continents' => $this->getContinents(),
            'professions' => array('architectus vel architecta',
                                   'discipulus vel discipula scholaris',
                                   'discipulus universitarius vel discipula universitaria',
                                   'iuris consultus vel iuris consulta',
                                   'magister vel magistra linguae Latinae in schola inferiori',
                                   'magister vel magistra aliarum disciplinarum in schola inferiori',
                                   'medicus vel medica',
                                   'monachus vel monacha',
                                   'professor universitarius vel profestrix universitaria linguae Latinae',
                                   'professor universitarius vel profestrix universitaria aliarum disciplinarum', 'sacerdos',
                                   'rude donatus vel donata. Eram...'
            ),
            'religions' => array('fidem Christianam [Catholicam, Lutheranam, Calvinianam, aliam] profiteor',
                                 'fidem Iudaeam profiteor',
                                 'fidem Mahometanam profiteor',
                                 'fidem Buddhisticam profiteor',
                                 'Atheus vel athea sum'
            ),
            'languages' => array(
                'Anglicam (sermo patrius est, optime edidici, mediocriter didici, leviter attigi)',
                'Arabicam (sermo patrius est, optime edidici, mediocriter didici, leviter attigi)',
                'Batavicam (sermo patrius est, optime edidici, mediocriter didici, leviter attigi)',
                'Catalaunicam (sermo patrius est, optime edidici, mediocriter didici, leviter attigi)',
                'Daco-Romanicam (sermo patrius est, optime edidici, mediocriter didici, leviter attigi)',
                'Esperanticam (sermo patrius est, optime edidici, mediocriter didici, leviter attigi)',
                'Gallicam (sermo patrius est, optime edidici, mediocriter didici, leviter attigi)',
                'Germanicam (sermo patrius est, optime edidici, mediocriter didici, leviter attigi)',
                'Graecam antiquiorem (sermo patrius est, optime edidici, mediocriter didici, leviter attigi)',
                'Graecam recentiorem (sermo patrius est, optime edidici, mediocriter didici, leviter attigi)',
                'Hebraicam (sermo patrius est, optime edidici, mediocriter didici, leviter attigi)',
                'Hindustanicam (sermo patrius est, optime edidici, mediocriter didici, leviter attigi)',
                'Hispanicam (sermo patrius est, optime edidici, mediocriter didici, leviter attigi)',
                'Hungaricam (sermo patrius est, optime edidici, mediocriter didici, leviter attigi)',
                'Iaponicam (sermo patrius est, optime edidici, mediocriter didici, leviter attigi)',
                'Italicam (sermo patrius est, optime edidici, mediocriter didici, leviter attigi)',
                'Lusitanicam (sermo patrius est, optime edidici, mediocriter didici, leviter attigi)',
                'Polonicam (sermo patrius est, optime edidici, mediocriter didici, leviter attigi)',
                'Russicam (sermo patrius est, optime edidici, mediocriter didici, leviter attigi)',
                'Sanscritam (sermo patrius est, optime edidici, mediocriter didici, leviter attigi)',
                'Sinensem (sermo patrius est, optime edidici, mediocriter didici, leviter attigi)',
                'Vasconicam (sermo patrius est, optime edidici, mediocriter didici, leviter attigi)'
            ),
            'seminars' => array(
                'Biduum Angelopolitanum societatis q.i. SALVI (semel, bis, ter, pluries)',
                'Conventiculum Bostoniense (semel, bis, ter, pluries)',
                'Conventiculum Dickinsoniense (semel, bis, ter, pluries)',
                'Conventiculum Lexintoniense (semel, bis, ter, pluries)',
                'Conventiculum Maretiolense (semel, bis, ter, pluries)',
                'Conventus Academiae Latinitati Fovendae (semel, bis, ter, pluries)',
                'Rusticatio Tironum societatis q.i. SALVI (semel, bis, ter, pluries)',
                'Rusticatio Omnium societatis q.i. SALVI (semel, bis, ter, pluries)',
                'Rusticatio Veteranorum societatis q.i. SALVI (semel, bis, ter, pluries)',
                'Scholae Romanae Instituti q.i. POLIS (semel, bis, ter, pluries)',
                'Septimana Latina Europaea (semel, bis, ter, pluries)',
                'Seminarium Romanum Instituti q.i. PAIDEIA (semel, bis, ter, pluries)',
                'Seminarium Lutetiense Instituti q.i. PAIDEIA (semel, bis, ter, pluries)',
                'Seminarium Caelum Matriti (semel, bis, ter, pluries)',
                'Feriae Ferigoletenses (semel, bis, ter, pluries)',
                'Seminarium societatis q.i. LUPA (semel, bis, ter, pluries)',
                'Scholae Aestivae Latinitatis Reginaldi Foster (semel, bis, ter, pluries)',
                'Scholae Aestivae Montellenses (semel, bis, ter, pluries)'
            ),
            'textbooks' => array(
                'Cambridge Latin Course: vol.1, vol.2, vol.3, vol.4, vol. 5',
                'Ecce Romani: vol.1, vol.2, vol.3',
                'Jenney’s Latin: vol.1, vol.2, vol.3, vol.4',
                'Latin for the New Millennium (T.Tunberg & M.Minkova): vol.1, vol.2., vol.3',
                'Lingua Latina per se Illustrata (H.Øerberg): vol.1, vol.2',
                'Oxford Latin Course : vol.1, vol.2, vol.3',
                'Le Latin sans Peine, Assimil (C.Desessard)',
                'Wheelock’s Latin (Wheelock)'
            ),
            'howStart' => array(
                'parentis ductu',
                'solus vel sola libris aliisque instrumentis utens',
                'magistri ductu in schola inferiori vel in universitate studiorum'
            ),
            'ageStarted' => $this->getAges(),
            'howLearned' => array(
                'parentis ductu',
                'solus vel sola libris aliisque instrumentis utens',
                'magistri ductu in schola inferiori vel in universitate studiorum'
            ),
            'howUse' => array('legendo',
                              'loquendo',
                              'scribendo',
                              'auscultando'
            ),
            'howLong' => $this->getHowLong(),
            'universities' => $this->getUniversities()
        );
        $this->session->set_userdata($staticData);
        //$this->setValidation();
    }

    function setValidation() {
        $this->form_validation->set_rules('FName', 'First name', 'required', array( 'required' => 'Haec lacuna est explenda.'));
        $this->form_validation->set_rules('LName', 'Last name', 'required', array( 'required' => 'Haec lacuna est explenda.'));
        $this->form_validation->set_rules('Gender', 'gender', 'required', array( 'required' => 'Haec lacuna est explenda.'));
        $this->form_validation->set_rules('Ethnicity', 'ethnicities', 'required', array( 'required' => 'Haec lacuna est explenda.'));
        $this->form_validation->set_rules('Age', 'year of birth', 'required', array( 'required' => 'Haec lacuna est explenda.'));
        $this->form_validation->set_rules('ContinentOfBirthId', 'continent', 'required', array( 'required' => 'Haec lacuna est explenda.'));
        $this->form_validation->set_rules('ContinentOfResidenceId', 'Continent of residence', 'required', array( 'required' => 'Haec lacuna est explenda.'));
        $this->form_validation->set_rules('CityOfResidenceId', 'city of residence', 'required', array( 'required' => 'Haec lacuna est explenda.'));
        $this->form_validation->set_rules('Profession', 'professions', 'required', array( 'required' => 'Haec lacuna est explenda.'));
        $this->form_validation->set_rules('Religion', 'religion', 'required', array( 'required' => 'Haec lacuna est explenda.'));
        $this->form_validation->set_rules('AgeStart', 'age you started learning latin', 'required', array( 'required' => 'Haec lacuna est explenda.'));
        $this->form_validation->set_rules('HowLearn', 'how you started learning latin', 'required', array( 'required' => 'Haec lacuna est explenda.'));
        //$this->form_validation->set_rules('HowUse', 'main use for latin', 'required', 'Please select your main use for latin');
        $this->form_validation->set_rules('YearsRead', "reading latin", 'required', array( 'required' => 'Haec lacuna est explenda.'));
        $this->form_validation->set_rules('YearsSpeak', "speaking latin", 'required', array( 'required' => 'Haec lacuna est explenda.'));
        $this->form_validation->set_rules('YearsWrite', "writing latin", 'required', array( 'required' => 'Haec lacuna est explenda.'));
        //$this->form_validation->set_rules('YearsListen', "listening to latin", 'required', "Please enter how many years you've been listening to latin");
        // Need to make rule whereby only one of otherSchool or univContinent is required.
        $this->form_validation->set_rules('schoolContinent', 'school continent', 'required', array( 'required' => 'Haec lacuna est explenda.'));
        //        $this->form_validation->set_rules('SeminarsAttended',"seminar",'required',"Please select a seminar or 'other'");
        // Need to make rule whereby only one of otherSchool or univContinent is required.
        $this->form_validation->set_rules('Textbook',"textbook",'required', array( 'required' => 'Haec lacuna est explenda.'));
        //Email is optional
        //$this->form_validation->set_rules('EmailAddress',"email",'required',"Please enter an email address");
        //Comments are optional
        //$this->form_validation->set_rules('comment',"comment",'required',"Please enter an email address");
    }

    function getYears() {
        // Fill with years since 1950 and 10 years ago.
        $years = array();
        $currentYear = date("Y");
        for ($i=1920;$i<=$currentYear;$i++) {
            array_push($years, $i);
        }
        return $years;
    }

    function getAges() {
        // Fill with years since 1950 and 10 years ago.
        $ages = array();
        $lastAge = 90;
        for ($i=1;$i<$lastAge + 1;$i++) {
            array_push($ages, $i);
        }
        return $ages;
    }

    function getHowLong() {
        //Fill with ints from 1 to 50.
        $years = array();

        for ($i=1;$i<51;$i++) {
            array_push($years, $i);
        }
        return $years;
    }

    function getContinents() {
        $continents = $this->census_model->get_continents();
        return $continents;
    }

    function getUniversities() {
        $schools = array();

        $schools = $this->census_model->get_universities();
        return $schools;
    }

    public function getUniversityCountries() {
        $countries = array();
        $countries = $this->census_model->get_countries_by_continent_id($_REQUEST['continentId']);
        $output = "<div class='col-xs-3'>";
        $output .= "<p>";
        $output .= "<select class='select' id='".$_REQUEST['countriesSelectId']. "' name='".$_REQUEST['countriesSelectId']."' ";
        $output .= "  onchange=showUniversitiesByCountry('".$_REQUEST['countriesSelectId']."','".$_REQUEST['citiesSelectId']."','".$_REQUEST['citiesAreaId']."')>";
        $output .= "<option selected='selected' value='0'>elige</option>";

        foreach ($countries as $key => $value) {
            $output .= "<option value=".$value['id'].">";
            $output .= $value['name']."</option>";
        }

        $output .= "</select>";
        $output .= "</p>";
        $output .= "</div>";
        echo $output;
    }

    //getUniversitiesByCountry
    public function getUniversitiesByCountry($id = 0) {
        $cities = array();
        $id = $_REQUEST['countryId'];
        $cities = $this->census_model->get_universities_by_country_id($id);
        $output = "<div class='col-xs-3'>";
        $output .= "<p>";
        //$output .= "<h3>Please select a school:</h3>";
        $output .= "<select class='select' id='".$_REQUEST['citiesSelectId']."' ".
                   "name='".$_REQUEST['countriesSelectId']."' "."'>";
        $output .= "<option selected='selected' value='0'>select</option>";
        foreach ($cities as $key => $value) {
            $output .= "<option value=".$value['id'].">";
            $output .= $value['name']."</option>";
        }
        $output .= "<option value='other'>Scribe nomen:</option>";
        $output .= "</select>";
        $output .= "</p>";
        $output .= "</div>";
        echo $output;
    }

    /* args: citiesToo: 'yes|no', indicates whether the cities for the desired country are
       shown.
       countriesAreaId:  The id of the area where the countries are to be displayed
       citiesAreaId: The id of the area to which the cities shouid be rendered.
       continentsArea:  The id of the continent that was selected at time of call.
     */
    public function getCountries() {
        $countries = array();
        $countries = $this->census_model->get_countries_by_continent_id($_REQUEST['continentId']);
        $output = "<div class='col-xs-3'>";
        $output .= "<p>";
        //$output .= "<h3>Please select a country:</h3>";
        if ($_REQUEST['citiesToo'] == 'yes') {
            $output .= "<select class='select' id='".$_REQUEST['countriesSelectId']."'";
            $output .= " name='".$_REQUEST['countriesSelectId']."'";
            $output .= " onchange=showCities('".$_REQUEST['countriesSelectId']."','".$_REQUEST['citiesSelectId']."','".$_REQUEST['citiesAreaId']."')>";
            $output .= "<option selected='selected' value='0'>elige</option>";
        } else {
            $output .= "<select class='select' name='";
            $output .= $_REQUEST['countriesSelectId']."'>";
            $output .= "<option selected='selected' value='0'>elige</option>";
        }

        foreach ($countries as $key => $value) {
            $output .= "<option value=".$value['id'].">";
            $output .= $value['name']."</option>";
        }

        $output .= "</select>";
        $output .= "</p>";
        $output .= "</div>";
        echo $output;
    }

    public function getCities($id = 0) {
        $cities = array();
        $id = $_REQUEST['countryId'];
        $cities = $this->census_model->get_cities_by_country_id($id);
        $output = "<div class='col-xs-3'>";
        $output .= "<p>";
        //$output .= "<h3>Please select a city:</h3>";
        $output .= "<select class='select' name='".$_REQUEST['citiesSelectId']."' id='".$_REQUEST['citiesSelectId']."'>";
        $output .= "<option selected='selected' value='0'>elige</option>";
        foreach ($cities as $key => $value) {
            $output .= "<option value=".$value['id'].">";
            $output .= $value['name']."</option>";
        }
        $output .= "<option value='other'>Scribe nomen:</option>";
        $output .= "</select>";
        $output .= "</p>";
        $output .= "</div>";
        echo $output;
    }
}
