<!doctype html>
<html>
  <head>
    <base href="<?php echo site_url(); ?>" />
    <title>CENSVS LATINVS </title>
    <link rel=StyleSheet href="style.css" type="text/css">
    <link rel=StyleSheet href="resources/jquery-ui-1.12.1.custom/jquery-ui.min.css" type="text/css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  </head>
  <body>
    <div class="bg">
    <div class="fg">
      <center>
        <div class="primaryhead"><?php echo $title; ?></div>
        <div class="secondhead"><?php echo "---- ANNI MMXVIII ----"; ?></div>
      </center>
        <div class="floatright">
          <a href="http://www.academialatinitatifovendae.org/Academialatinitatifovendae/ALF.html">
            <img src="images/alflogo.gif">
          </a>
        </div>
        <div class="floatleft">
          <a href="http://www.wwu.edu/mcl/classical">
            <img src="images/wwulogonew4.gif">
          </a>
        </div>
        <br/>
        <br/>
        <hr width='100%'/>
